const express = require('express');
const graphqlHTTP = require('express-graphql');
const mongoose = require('mongoose');
const schema = require('./schema/schema');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

// allow cross origin access
app.use(cors());

app.use(function(req, res, next) {
    // res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}), bodyParser.json());

mongoose.connect('mongodb://admin:admin123@ds139614.mlab.com:39614/sb-gql-ninja', { useNewUrlParser: true });
mongoose.connection.once('open', ()=>{
    console.log('mongoose db connected');
})

const port = process.env.PORT || 5000;

app.listen(
   port, () => console.info(
      `Server started on port ${port}`
   )
);
