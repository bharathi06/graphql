const graphql = require('graphql');
const Book = require('../models/books');

const{
    GraphQLString,
    GraphQLID,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLList,
    GraphQLInt
} = graphql;

const BookType = new GraphQLObjectType({
    name: 'Books',
    fields: () => ({
        name: { type: GraphQLString },
        genre: { type: GraphQLString },
        authorid: { type: GraphQLString }
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        book: {
            type: BookType,
            args: {id: {type: GraphQLString }},
            resolve(parent, args){
                //
            }
        },
        books: {
            type: new GraphQLList(BookType),
            resolve(parent, args){
                return Book.find({});
            }
        }
    }
})

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields:{
        addBook:{
            type: BookType,
            args:{
                name: { type: GraphQLString },
                genre: { type: GraphQLString },
                authorid: { type: GraphQLString }
            },
            resolve(parent, args){
                let book = new Book({
                    name: args.name,
                    genre: args.genre,
                    authorid: args.authorid
                });
                return book.save();
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});