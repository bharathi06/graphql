import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// Components
import BookList from './components/BookList';

// Apollo client setup
const client = new ApolloClient({
  uri: 'http://localhost:5000/graphql',
  // fetchOptions: { mode: "no-cors", }
})

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <h1>Test App</h1>
          <BookList/>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
