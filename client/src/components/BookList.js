import React, { Component } from 'react';

import { gql } from 'apollo-boost';
import { graphql } from 'react-apollo';

const getBooksQuery = gql`
{
  books{
    name
    genre
  }
}
`

class BookList extends Component {
  displayBooks(){
    var data = this.props.data;
    if(data.loading){
      return (<div>Loading books...</div>);
    } else {
      console.log(this.props.data.books)
      // return this.props.data.map(function(book, i){
      //   return <li key={i}>{book.name}</li>
      // })
    }
  }
  render() {
    return (
      <div className="App">
        <h1>Ninja's List</h1>
        <ul>
          {this.displayBooks()}
        </ul>
      </div>
    );
  }
}

export default graphql(getBooksQuery)(BookList);
